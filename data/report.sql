-- Adminer 4.3.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `report_category`;
CREATE TABLE `report_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `report_category` (`id`, `name`) VALUES
(1,	'Features'),
(2,	'Disadvantages'),
(3,	'Other');

DROP TABLE IF EXISTS `report_template`;
CREATE TABLE `report_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('string','text','editor','checkbox','radiobox','select','multiselect','image','point') COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adds` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `report_template_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `report_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `report_template` (`id`, `type`, `category_id`, `name`, `hint`, `adds`) VALUES
(1,	'string',	1,	'Slogan',	'Loud phrase',	''),
(2,	'checkbox',	1,	'Immortal',	'Can I kill a unit?',	'Yes\r\nNo'),
(3,	'checkbox',	1,	'Price range',	'',	'0-100\r\n101-250\r\n251-500\r\n501-1000'),
(4,	'editor',	1,	'Full review',	'',	''),
(5,	'text',	1,	'Entry',	'',	''),
(6,	'multiselect',	1,	'Who wrote the book \"Design Patterns\"',	'',	'Gamma\r\nHelm\r\nJohnson\r\nVlissides\r\nLennon'),
(7,	'select',	1,	'Use as a background',	'',	'color\r\nimage\r\ntransparent'),
(8,	'image',	1,	'Logo',	'url to image file',	''),
(9,	'point',	1,	'Where are you now?',	'GPS point. Lat long separated with space',	'');

DROP TABLE IF EXISTS `report_unit`;
CREATE TABLE `report_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `report_unit` (`id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1,	'Mr. First',	NULL,	NULL,	1491913056,	101),
(2,	'Mr. Second',	1491910799,	101,	1491910799,	101);

DROP TABLE IF EXISTS `report_value`;
CREATE TABLE `report_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `template_id` (`template_id`),
  KEY `unit_id` (`unit_id`),
  CONSTRAINT `report_value_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `report_template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `report_value_ibfk_3` FOREIGN KEY (`unit_id`) REFERENCES `report_unit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2017-04-12 09:25:18
