<?php

namespace app\modules\report\forms;

use Yii;
use yii\base\Model;

/**
 *
 */
class StringForm extends Model
{
    public $label;
    public $value;
    public function rules()
    {
        return [
            [['label', 'value'], 'required'],
            [['label', 'value'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'label' => 'Label',
            'value' => 'Value',
        ];
    }
}
