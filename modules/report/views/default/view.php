<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Unit */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'created_at',
                'label' => 'Created',
                'format' => 'html',
                'value' => function ($data){

                    return 'User: ' . User::findNameById($data->created_by)
                        . '<br />' . Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Updated',
                'format' => 'html',
                'value' => function ($data){
                    return 'User: ' . User::findNameById($data->updated_by)
                        . '<br />' . Yii::$app->formatter->asDatetime($data->updated_at);
                },
                'filter' => false
            ],
        ],
    ]) ?>

</div>
