<?php

use app\modules\report\forms\StringForm;
use app\modules\report\models\Template;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Unit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            addParam <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <?php foreach(Template::getTypes() as $id => $type){ ?>
                <li><a data-id="<?= $id ?>" href=""><?= $type ?></a></li>
            <?php } ?>
        </ul>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="hidden templates">

    <div class="string">
        <?php $formModel = new StringForm() ?>
        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <?= $form->field($formModel, 'label')->textInput(['maxlength' => true]) ?>
            <?= $form->field($formModel, 'value')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>

<?php

$js = <<<JS
    $('body').on('click', '.dropdown-menu a', function() {
        var id = $(this).data('id');
        $(this).closest('.btn-group').removeClass('open');
        console.log(id);
        return false;
    });
JS;
$this->registerJs($js);
