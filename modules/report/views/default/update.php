<?php

use app\modules\report\models\Param;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Unit */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Unit',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="unit-update container">

    <div class="row">
        <div class="col-md-6">

            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
        <div class="col-md-6">

        </div>
    </div>

</div>