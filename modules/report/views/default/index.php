<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\report\models\UnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Units');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Unit'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '75'],
            ],
            'name',
            [
                'attribute' => 'created_at',
                'label' => 'Created',
                'format' => 'html',
                'value' => function ($data, $key, $index, $column){

                    return 'User: ' . User::findNameById($data->created_by)
                        . '<br />' . Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Updated',
                'format' => 'html',
                'value' => function ($data, $key, $index, $column){
                    return 'User: ' . User::findNameById($data->updated_by)
                        . '<br />' . Yii::$app->formatter->asDatetime($data->updated_at);
                },
                'filter' => false
            ],

            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
