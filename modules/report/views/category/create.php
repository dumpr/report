<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Category */

$this->title = Yii::t('app', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Templates'), 'url' => ['/report/template/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create container">

    <div class="row">
        <div class="col-md-6">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

        </div>
        <div class="col-md-6">

        </div>
    </div>

</div>
