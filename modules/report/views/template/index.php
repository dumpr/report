<?php

use app\modules\report\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\report\models\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Templates');
$this->params['breadcrumbs'][] = $this->title;

$caterories = ArrayHelper::map(Category::find()->all(), 'id', 'name');

?>
<div class="template-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Template'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Edit Categories'), ['/report/category'], ['class' => 'btn btn-default']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['style' => 'width:75px'],
            ],
            [
                'attribute' => 'type',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'type',
                    $searchModel::getTypes(),
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($data) {
                    return $data->typeName;
                }
            ],
            [
                'attribute' => 'category_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'category_id',
                    $caterories,
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($data) use ($caterories) {
                    return $caterories[$data->category_id];
                }
            ],
            'name',
            'hint',
            'adds:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}&nbsp;{update}&nbsp;{delete}',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'options' => ['style' => 'width: 145px'],
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->id],
                            [
                                'title' => 'View',
                                'class' => 'btn btn-default',
                            ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            ['update', 'id' => $model->id],
                            [
                                'title' => 'Update',
                                'class' => 'btn btn-default',
                            ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->id],
                            [
                                'title' => 'Delete',
                                'data-confirm' => 'All features of the objects created by this template will be deleted. Proceed?',
                                'data-method' => 'post',
                                'class' => 'btn btn-default',
                            ]);
                    }
                ],
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
