<?php

namespace app\modules\report\models;

use kotchuprik\sortable\behaviors\Sortable;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%template}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $hint
 * @property string $type
 * @property string $position
 *
 * @property Value[] $values
 */
class Template extends ActiveRecord
{

    // 'string','checkbox','radiobox','select','multiselect','image','point'
    const T_STRING = 'string';
    const T_TEXT = 'text';
    const T_EDITOR = 'editor';
    const T_CHECKBOX = 'checkbox';
    const T_RADIOBOX = 'radiobox';
    const T_SELECT = 'select';
    const T_MULTISELECT = 'multiselect';
    const T_IMAGE = 'image';
    const T_POINT = 'point';

    public static function getTypes()
    {
        return [
            self::T_STRING => 'String',
            self::T_TEXT => 'Text',
            self::T_EDITOR => 'Editor',
            self::T_CHECKBOX => 'Checkbox',
            self::T_RADIOBOX => 'Radiobox',
            self::T_SELECT => 'Select',
            self::T_MULTISELECT => 'Multiselect',
            self::T_IMAGE => 'Image',
            self::T_POINT => 'Point',
        ];
    }

    public function getTypeName()
    {
        return ArrayHelper::getValue(self::getTypes(), $this->type, self::getTypes()[self::T_STRING]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%template}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                /*'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['updated_at','created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],*/
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
            ],
            'sortable' => [
                'class' => Sortable::className(),
                'query' => self::find(),
                'orderAttribute' => 'position',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'hint', 'type'], 'required'],
            [['type'], 'string'],
            [['name', 'hint'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'hint' => Yii::t('app', 'Hint'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(Value::className(), ['param_id' => 'id']);
    }
}
